/*
 *                   ___====-_  _-====___
 *             _--^^^#####//      \\#####^^^--_
 *          _-^##########// (    ) \\##########^-_
 *         -############//  |\^^/|  \\############-
 *       _/############//   (@::@)   \############\_
 *      /#############((     \\//     ))#############\
 *     -###############\\    (oo)    //###############-
 *    -#################\\  / VV \  //#################-
 *   -###################\\/      \//###################-
 *  _#/|##########/\######(   /\   )######/\##########|\#_
 *  |/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
 *  `  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
 *     `   `  `      `   / | |  | | \   '      '  '   '
 *                      (  | |  | |  )
 *                     __\ | |  | | /__
 *                    (vvv(VVV)(VVV)vvv)
 * 
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 *                神兽保佑            永无BUG
 * 
 * @Descripttion: 
 * @version: 
 * @Date: 2022-04-27 00:52:16
 * @LastEditors: huzhushan@126.com
 * @LastEditTime: 2022-04-27 00:57:21
 * @Author: huzhushan@126.com
 * @HomePage: https://huzhushan.gitee.io/vue3-element-admin
 * @Github: https://github.com/huzhushan/vue3-element-admin
 * @Donate: https://huzhushan.gitee.io/vue3-element-admin/donate/
 */

export default [
  {
    url: '/api/bank-account', // 请求地址
    method: 'get', // 请求方法
    timeout: 1000,
    response: ({ query }) => {
      // 响应内容
      return {
        code: 200,
        data: {
          list: [
            {
              id: 1,
              account: 'Cash',
              currency: 'SGD - Singapore Dollar',
              balance: '1,000.00',
            },
            {
              id: 2,
              account: 'UOB 23032098',
              currency: 'SGD - Singapore Dollar',
              balance: '56,800.00',
            },
          ],
          total: 100,
        },
      }
    },
  },
  {
    url: '/api/transaction', // 请求地址
    method: 'get', // 请求方法
    timeout: 1800,
    response: ({ query }) => {
      // 响应内容
      return {
        code: 200,
        data: {
          list: [
            {
              id: 1,
              number: 'RV-1900019',
              type: 'Money-In',
              paid: 'GHI Company',
              date: '982749274569',
              amount: 'SGD 500.00',
              amountType: '+',
            },
            {
              id: 2,
              number: 'RV-1900018',
              type: 'Money-Out',
              paid: 'ABC Company',
              date: '987779274569',
              amount: 'SGD 1,500.00',
              amountType: '-',
            },
          ],
          total: 100,
        },
      }
    },
  },
]
