## 克隆项目
```bash
  git clone https://gitee.com/HUZHUSHAN/ace-electronics-demo.git
```
## 安装依赖
```bash
  cd ace-electronics-demo
  npm install
```
## 运行项目
```bash
  npm start
```
